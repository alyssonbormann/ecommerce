﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ECommerce.Helpers
{
    public class FilesHelper
    {
        public static bool UploadFoto(HttpPostedFileBase arquivo, string pasta, string nome)
        {
            string caminho = string.Empty;
            //string pic = string.Empty;

            if (arquivo == null || string.IsNullOrEmpty(pasta) || string.IsNullOrEmpty(nome))
            {
                return false;
            }


            try
            {
                if (arquivo != null)
                {
                    //pic = Path.GetFileName(file.FileName);
                    caminho = Path.Combine(HttpContext.Current.Server.MapPath(pasta), nome);
                    arquivo.SaveAs(caminho);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        arquivo.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();
                    }

                }

                return true;
            }
            catch (Exception)
            {

                return false;
            }



        }
    }
}