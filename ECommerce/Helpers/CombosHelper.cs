﻿using ECommerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECommerce.Helpers
{
    public class CombosHelper : IDisposable
    {
        private static ECommerceContext db = new ECommerceContext();

        public static List<Departamento> GetDepartamentos()
        {
            var dep = db.Departamentos.ToList();
            dep.Add(new Departamento
            {
                DepartamentoId = 0,
                Nome = "[ Selecione um Departamento ]"
            });

            return dep = dep.OrderBy(c => c.Nome).ToList();
        }

        public static List<Cidade> GetCidades()
        {
            var cit = db.Cidades.ToList();
            cit.Add(new Cidade
            {
                CidadeId = 0,
                Nome = "[ Selecione uma Cidade ]"
            });

            return cit = cit.OrderBy(c => c.Nome).ToList();
        }

        public static List<Companhia> GetCompanhia()
        {
            var com = db.Companhias.ToList();
            com.Add(new Companhia
            {
                CompanhiaId = 0,
                Nome = "[ Selecione uma Companhia ]"
            });

            return com = com.OrderBy(c => c.Nome).ToList();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}