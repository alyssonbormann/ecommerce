﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ECommerce.Models
{
    public class Categoria
    {
        [Key]
        [Display(Name ="Categoria Id")]
        public int CategoriaId { get; set; }

        [Required(ErrorMessage ="O campo Descrição é requerido")]
        [Display(Name ="Descrição")]
        //[Index("Categoria_Descricao_CompanhiaId_Index", 2, IsUnique = true)]
        public string Descricao { get; set; }

        [Required(ErrorMessage = "O campo Distribuidora é requerido")]
        [Display(Name = "Distribuidora")]
        [Range(1, double.MaxValue, ErrorMessage ="Selecione uma distribuidora")]
       // [Index("Categoria_Descricao_CompanhiaId_Index", 1, IsUnique = true)]
        public int CompanhiaId { get; set; }

        public virtual Companhia Companhia { get; set; }

    }
}