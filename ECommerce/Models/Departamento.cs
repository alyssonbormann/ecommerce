﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ECommerce.Models
{
    public class Departamento
    {
        [Key]
        [Display(Name = "Departamento Id")]
        public int DepartamentoId { get; set; }

        [Required(ErrorMessage = "O campo Nome é requerido")]
        [Index("Departamento_Nome_Index", IsUnique = true)]
        [MaxLength(50,ErrorMessage ="O campo Nome recebe no máximo 50 caracteres")]
        [Display(Name = "Departamento")]
        public string Nome { get; set; }

        public virtual ICollection<Cidade> Cidades { get; set; }
        public virtual ICollection<Companhia> Companhia { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}