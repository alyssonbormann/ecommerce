﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace ECommerce.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }


        [Required(ErrorMessage = "O campo E-Mail é requerido")]
        [Index("User_Username_Index", IsUnique = true)]
        [MaxLength(250, ErrorMessage = "O campo E-Mail recebe no máximo 25 0 caracteres")]
        [Display(Name = "E-Mail")]
        public string UserName { get; set; }


        [Required(ErrorMessage = "O campo Nome é requerido")]
        [Index("User_Nome_Index", IsUnique = true)]
        [MaxLength(50, ErrorMessage = "O campo Nome recebe no máximo 50 caracteres")]
        [Display(Name = "Nome")] 
        public string Nome { get; set; }


        [Required(ErrorMessage = "O campo Sobrenome é requerido!!")]
        [MaxLength(50, ErrorMessage = "O campo Sobrenome recebe no máximo 50 caracteres")]
        [Display(Name = "Sobrenome")]
        public string Sobrenome { get; set; }


        [Required(ErrorMessage = "O campo Telefone é requerido!!")]
        [MaxLength(50, ErrorMessage = "O campo Telefone recebe no máximo 50 caracteres")]
        [Display(Name = "Telefone")]
        [DataType(DataType.PhoneNumber)]
        public string Telefone { get; set; }


        [Required(ErrorMessage = "O campo Endereço é requerido!!")]
        [MaxLength(100, ErrorMessage = "O campo Endereço recebe no máximo 50 caracteres")]
        [Display(Name = "Endereço")]
        public string Endereco { get; set; }


        [Display(Name = "Imagem")]
        [DataType(DataType.ImageUrl)]
        public string Imagem { get; set; }


        [NotMapped]
        public HttpPostedFileBase ImagemArquivo { get; set; }


        [Required(ErrorMessage = "O campo Departamento é requerido!!")]
        [Display(Name = "Departamento")]
        public int DepartamentoId { get; set; }


        [Required(ErrorMessage = "O campo Cidade é requerido!!")]
        [Display(Name = "Cidade")]
        public int CidadeId { get; set; }


        [Required(ErrorMessage = "O campo Cidade é requerido!!")]
        [Display(Name = "Companhia")]
        public int CompanhiaId { get; set; }


        [Display(Name = "Usuário")]
        public string NomeCompleto { get { return string.Format("{0} {1}", Nome, Sobrenome); } }


        public virtual Departamento Departamentos { get; set; }
        public virtual Cidade Cidades { get; set; }
        public virtual Companhia Companhia { get; set; }
    }
}