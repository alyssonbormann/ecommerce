﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ECommerce.Models
{
    public class ECommerceContext : DbContext
    {
        public ECommerceContext() : base("DefaultConnection")
        {

        }

        public System.Data.Entity.DbSet<ECommerce.Models.Departamento> Departamentos { get; set; }

        public System.Data.Entity.DbSet<ECommerce.Models.Cidade> Cidades { get; set; }

        public System.Data.Entity.DbSet<ECommerce.Models.Companhia> Companhias { get; set; }

        public System.Data.Entity.DbSet<ECommerce.Models.User> Users { get; set; }


        public System.Data.Entity.DbSet<ECommerce.Models.Categoria> Categorias { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}