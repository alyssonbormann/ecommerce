﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace ECommerce.Models
{
    public class Companhia
    {
        [Key]
        public int CompanhiaId { get; set; }


        [Required(ErrorMessage = "O campo Nome é requerido")]
        [Index("Departamento_Nome_Index", IsUnique = true)]
        [MaxLength(50, ErrorMessage = "O campo Nome recebe no máximo 50 caracteres")]
        [Display(Name = "Nome")]
        public string Nome { get; set; }


        [Required(ErrorMessage = "O campo Telefone é requerido")]
        [MaxLength(50, ErrorMessage = "O campo Telefone recebe no máximo 50 caracteres")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Telefone")]
        public string Telefone { get; set; }


        [Required(ErrorMessage = "O campo Endereço é requerido")]
        [MaxLength(100, ErrorMessage = "O campo Endereço recebe no máximo 100 caracteres")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Endereço")]
        public string Endereco { get; set; }


        [DataType(DataType.ImageUrl)]
        [Display(Name = "Imagem")]
        public string Logo { get; set; }


        [NotMapped]
        public HttpPostedFileBase LogoArquivo { get; set; }


        [Required(ErrorMessage = "O campo Deparmento é requerido")]
        [Display(Name="Departamento")]
        public int DepartamentoId { get; set; }


        [Required(ErrorMessage = "O campo Cidade é requerido")]
        [Display(Name="Cidade")]
        public int CidadeId { get; set; }


        public virtual Departamento Departamento { get; set; }
        public virtual Cidade Cidades { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<Categoria> Categoria { get; set; }

    }
}