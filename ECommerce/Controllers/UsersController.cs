﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ECommerce.Models;
using ECommerce.Helpers;
using static ECommerce.Helpers.UserHelper;

namespace ECommerce.Controllers
{
    public class UsersController : Controller
    {
        private ECommerceContext db = new ECommerceContext();

        // GET: Users
        public ActionResult Index()
        {
            var users = db.Users.Include(u => u.Cidades).Include(u => u.Companhia).Include(u => u.Departamentos);
            return View(users.ToList());
        }

        public JsonResult GetCidades(int departamentoId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var cidades = db.Cidades.Where(c => c.DepartamentoId == departamentoId);
            return Json(cidades);
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            ViewBag.CidadeId = new SelectList(CombosHelper.GetCidades(), "CidadeId", "Nome");
            ViewBag.CompanhiaId = new SelectList(CombosHelper.GetCompanhia(), "CompanhiaId", "Nome");
            ViewBag.DepartamentoId = new SelectList(CombosHelper.GetDepartamentos(), "DepartamentoId", "Nome");
            return View();
        }

        // POST: Users/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                db.SaveChanges();
                UsersHelper.CreateUserASP(user.UserName, "User");

                if (user.ImagemArquivo != null)
                {
                    var foto = string.Empty;
                    var pasta = "~/img/Users";
                    var arquivo = string.Format("{0}.jpg", user.UserId);

                    var response = FilesHelper.UploadFoto(user.ImagemArquivo, pasta, arquivo);
                    if (response)
                    {
                        foto = string.Format("{0}/{1}", pasta, arquivo);
                        user.Imagem = foto;
                        db.Entry(user).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                return RedirectToAction("Index");
            }
            ViewBag.CidadeId = new SelectList(CombosHelper.GetCidades(), "CidadeId", "Nome");
            ViewBag.CompanhiaId = new SelectList(CombosHelper.GetCompanhia(), "CompanhiaId", "Nome");
            ViewBag.DepartamentoId = new SelectList(CombosHelper.GetDepartamentos(), "DepartamentoId", "Nome");
            return View(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.CidadeId = new SelectList(CombosHelper.GetCidades(), "CidadeId", "Nome");
            ViewBag.CompanhiaId = new SelectList(CombosHelper.GetCompanhia(), "CompanhiaId", "Nome");
            ViewBag.DepartamentoId = new SelectList(CombosHelper.GetDepartamentos(), "DepartamentoId", "Nome");
            return View(user);
        }

        // POST: Users/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(User user)
        {
            if (ModelState.IsValid)
            {
                if (user.ImagemArquivo != null)
                {
                    var foto = string.Empty;
                    var pasta = "~/img/Users";
                    var arquivo = string.Format("{0}.jpg", user.UserId);

                    var response = FilesHelper.UploadFoto(user.ImagemArquivo, pasta, arquivo);
                    if (response)
                    {
                        foto = string.Format("{0}/{1}", pasta, arquivo);
                        user.Imagem = foto;
                    }
                }

                var db2 = new ECommerceContext();
                var currentuser = db2.Users.Find(user.UserId);
                if (currentuser.UserName != user.UserName)
                {
                    UserHelper.UsersHelper.UpdateUserName(currentuser.UserName, user.UserName);
                }
                db2.Dispose();

                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                UserHelper.UsersHelper.UpdateUserName(user.Nome, "User");
                return RedirectToAction("Index");
            }
            ViewBag.CidadeId = new SelectList(CombosHelper.GetCidades(), "CidadeId", "Nome");
            ViewBag.CompanhiaId = new SelectList(CombosHelper.GetCompanhia(), "CompanhiaId", "Nome");
            ViewBag.DepartamentoId = new SelectList(CombosHelper.GetDepartamentos(), "DepartamentoId", "Nome");
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
