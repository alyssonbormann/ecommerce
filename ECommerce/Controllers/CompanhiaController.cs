﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ECommerce.Models;
using ECommerce.Helpers;

namespace ECommerce.Controllers
{
    public class CompanhiaController : Controller
    {
        private ECommerceContext db = new ECommerceContext();

        // GET: Companhia
        public ActionResult Index()
        {
            var companhias = db.Companhias.Include(c => c.Cidades).Include(c => c.Departamento);
            return View(companhias.ToList());
        }

        public JsonResult GetCidades(int departamentoId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var cidades = db.Cidades.Where(c => c.DepartamentoId == departamentoId);
            return Json(cidades);
        }

        // GET: Companhia/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Companhia companhia = db.Companhias.Find(id);
            if (companhia == null)
            {
                return HttpNotFound();
            }
            return View(companhia);
        }

        // GET: Companhia/Create
        public ActionResult Create()
        {
            ViewBag.CidadeId = new SelectList(CombosHelper.GetCidades(), "CidadeId", "Nome");
            ViewBag.DepartamentoId = new SelectList(CombosHelper.GetDepartamentos(), "DepartamentoId", "Nome");
            return View();
        }

        // POST: Companhia/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Companhia companhia)
        {
            if (ModelState.IsValid)
            {
                db.Companhias.Add(companhia);
                db.SaveChanges();

                if (companhia.LogoArquivo != null)
                {
                    var foto = string.Empty;
                    var pasta = "~/img/Logos";
                    var arquivo = string.Format("{0}.jpg", companhia.CompanhiaId);

                    var response = FilesHelper.UploadFoto(companhia.LogoArquivo, pasta, arquivo);
                    if (response)
                    {
                        foto = string.Format("{0}/{1}", pasta, arquivo);
                        companhia.Logo = foto;
                        db.Entry(companhia).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                }

                return RedirectToAction("Index");
            }

            ViewBag.CidadeId = new SelectList(CombosHelper.GetCidades(), "CidadeId", "Nome", companhia.CidadeId);
            ViewBag.DepartamentoId = new SelectList(CombosHelper.GetDepartamentos(), "DepartamentoId", "Nome", companhia.DepartamentoId);
            return View(companhia);
        }

        // GET: Companhia/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Companhia companhia = db.Companhias.Find(id);
            if (companhia == null)
            {
                return HttpNotFound();
            }
            ViewBag.CidadeId = new SelectList(CombosHelper.GetCidades(), "CidadeId", "Nome", companhia.CidadeId);
            ViewBag.DepartamentoId = new SelectList(CombosHelper.GetCidades(), "DepartamentoId", "Nome", companhia.DepartamentoId);
            return View(companhia);
        }

        // POST: Companhia/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CompanhiaId,Nome,Telefone,Endereco,Logo,DepartamentoId,CidadeId")] Companhia companhia)
        {
            if (companhia.LogoArquivo != null)
            {
                var foto = string.Empty;
                var pasta = "~/img/Logos";
                var arquivo = string.Format("{0}.jpg", companhia.CompanhiaId);

                var response = FilesHelper.UploadFoto(companhia.LogoArquivo, pasta, arquivo);
                if (response)
                {
                    foto = string.Format("{0}/{1}", pasta, arquivo);
                    companhia.Logo = foto;
                }

                db.Entry(companhia).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }



            ViewBag.CidadeId = new SelectList(CombosHelper.GetCidades(), "CidadeId", "Nome", companhia.CidadeId);
            ViewBag.DepartamentoId = new SelectList(CombosHelper.GetCidades(), "DepartamentoId", "Nome", companhia.DepartamentoId);
            return View(companhia);
        }

        // GET: Companhia/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Companhia companhia = db.Companhias.Find(id);
            if (companhia == null)
            {
                return HttpNotFound();
            }
            return View(companhia);
        }

        // POST: Companhia/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Companhia companhia = db.Companhias.Find(id);
            db.Companhias.Remove(companhia);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
